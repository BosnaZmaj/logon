# Logon
The Script creates any amount of drives, in the sample creates two, and automatically mounts them for user. Any drive letter could be used as long as it is not already used by the system. In the sample I used S: and P:

For the script to work, a DFS Namespace has to be created.

In the P:/ the script will look for a folder Users and then for a Folder based on username. The AD User name and the Folder name have to match otherwise there will be an error. 

Replace DFS Namespace within sript with your own DFS Namespace. 
