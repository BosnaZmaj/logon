@ECHO off

REM ********************************************************
REM REQUIRES DFS NAMESPACE TO BE SETUP.
REM REPLACE DFS NAMESPACE BELOW WITH YOUR DFS NAMESPACE
REM YOU CAN USE ANY DRIVE LETTER THAT IS NOT ALREADY USED
REM BY SYSTEM
REM ********************************************************

REM GLOBAL SECTION
REM ********************************************************

REM @NET USE S: /DEL 
REM @NET USE P: /DEL 

REM ********************************************************
REM END GLOBAL SECTION
REM ********************************************************


REM ********************************************************
REM SECTION TO MAP "S" DRIVE TO SHARE BASED ON MACHINE NAME
REM ********************************************************

@NET USE S: \\DFS NAMESPACE\share
@NET USE P: \\DFS NAMESPACE\Users\%username%

REM ********************************************************
REM END MAP "S" DRIVE TO SHARE SECTION
REM ********************************************************

:END
